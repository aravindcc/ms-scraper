## To get started: `yarn`

### `yarn run watch`

Runs the app in development mode.<br>
Then follow these instructions to see your app:

1. Open **chrome://extensions**
2. Check the **Developer mode** checkbox
3. Click on the **Load unpacked extension** button
4. Select the folder **my-extension/build**

### `yarn run build`

Builds the app for production to the build folder.<br>
Zip the build folder and your app is ready to be published on Chrome Web Store.

### `yarn run format`

Formats all the HTML, CSS, JavaScript, TypeScript and JSON files.

This project was bootstrapped with [Chrome Extension CLI](https://github.com/dutiyesh/chrome-extension-cli)

yarn
