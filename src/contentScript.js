import { PDFDocument } from "pdf-lib";
import { DOMParser } from "xmldom";
import JSZip from "jszip";
import FileSaver from "file-saver";
("use strict");

const parser = new DOMParser();

async function get_html_doc(url) {
  const data = await fetch(url);
  const myString = await data.text();
  var myRegexp = /(<html.*>(.|\n)*<\/html>)/g;
  var text = myRegexp.exec(myString)[0];
  const e = parser.parseFromString(text);
  return e;
}

function onEvent(msg, document, wantsQuestions) {
  const a = document.getElementById("content").getElementsByTagName("div");

  var innerContent = undefined;
  for (var i = 0; i < a.length; i++) {
    if (a[i].getAttribute("class") == "campl-content-container") {
      innerContent = a[i];
      break;
    }
  }

  if (!innerContent) {
    alert("couldn't find content container!");
    return;
  }

  const b = innerContent.getElementsByTagName("a");
  const heading = innerContent
    .getElementsByTagName("h1")[0]
    .childNodes[0].nodeValue.replace("Past exam papers: ", "");
  const links = [];

  if (msg === "report_back") {
    for (var i = 0; i < b.length; i++) {
      const e = b[i];
      if (wantsQuestions) {
        if (
          e.childNodes[0].nodeValue.includes("Paper") &&
          e.childNodes[0].nodeValue.includes("Question")
        ) {
          links.push(e.getAttribute("href"));
        }
      } else if (e.childNodes[0].nodeValue == "solution notes") {
        links.push(e.getAttribute("href"));
      }
    }
  } else if (msg === "get_courses") {
    for (var i = 0; i < b.length; i++) {
      const e = b[i];
      if (e.parentElement.nodeName == "B") {
        links.push(e.getAttribute("href"));
      }
    }
  } else if (msg === "get_past_paper_link") {
    for (var i = 0; i < b.length; i++) {
      const e = b[i];
      if (e.childNodes[0].nodeValue == "Past exam questions") {
        links.push(e.getAttribute("href"));
      }
    }
  }

  return { links, heading };
}

chrome.runtime.onMessage.addListener(function (msg, sender, sendResponse) {
  if (msg.text === "get_courses") {
    const { links } = onEvent("get_courses", document);
    fetchPastPapers(links);
  } else if (msg.text == "get_single") {
    fetchPastPapers([], document); // TODO: change to saving
  }
});

async function fetchPastPaperPage(link) {
  try {
    const document = await get_html_doc(link);
    const { links, heading } = onEvent("get_past_paper_link", document);
    if (links.length == 0) throw Error("no past paper link");
    return links[0];
  } catch (err) {
    console.error(err);
    return undefined;
  }
}

function getPaperName(string) {
  var hold = "";
  var out = [];
  for (const c of string) {
    if (c >= "0" && c <= "9") {
      hold += c;
    } else if (hold != "") {
      out.push(parseInt(hold).toString());
      hold = "";
    }
  }
  if (hold != "") {
    out.push(parseInt(hold).toString());
    hold = "";
  }
  return out.join("-");
}

async function fetchAll(urls, wants_questions, folder, zip) {
  try {
    const data = await Promise.all(urls.map((url) => fetch(url)));
    const ext = await Promise.all(data.map((e) => e.arrayBuffer()));
    for (var i = 0; i < ext.length; i++) {
      const e = ext[i];
      const doc = await PDFDocument.load(e);
      const pdfBytes = await doc.save();
      var name = urls[i]
        .split("/")
        .pop()
        .replace(".pdf", "")
        .replace("-solutions", "");
      name = getPaperName(name);
      const filename = wants_questions ? "qs.pdf" : "ms.pdf";
      const path = `${folder}/${name}/${filename}`;
      zip.file(path, pdfBytes, { binary: true, createFolders: true });
    }
  } catch (err) {
    console.log(err);
  }
}

async function fetchAllPastLinks(url, wants_questions, zip) {
  if (!url) return;
  const document = await get_html_doc(url);
  const { links, heading: name } = onEvent(
    "report_back",
    document,
    wants_questions
  );
  if (wants_questions) {
    for (var i = 0; i < links.length; i++) {
      links[i] =
        "https://www.cl.cam.ac.uk/teaching/exams/pastpapers/" + links[i];
    }
  }
  await fetchAll(links, wants_questions, name, zip);
}

async function fetchPastPapers(urls, document = undefined) {
  try {
    const zip = new JSZip();
    var links;
    if (document == undefined) {
      links = await Promise.all(urls.map((url) => fetchPastPaperPage(url)));
    } else {
      links = [document.URL];
    }

    await Promise.all(links.map((link) => fetchAllPastLinks(link, true, zip)));
    await Promise.all(links.map((link) => fetchAllPastLinks(link, false, zip)));

    zip.generateAsync({ type: "blob" }).then(function (content) {
      FileSaver.saveAs(content, "download.zip");
    });
  } catch (err) {
    console.log(err);
  }
}
